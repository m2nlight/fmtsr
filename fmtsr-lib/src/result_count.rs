#[derive(Debug)]
pub struct ResultCount {
    pub success: u32,
    pub failure: u32,
    pub cost: u32,
}

impl ResultCount {
    pub fn new() -> ResultCount {
        ResultCount {
            success: 0,
            failure: 0,
            cost: 0,
        }
    }

    pub fn plus_success(&mut self) {
        self.success += 1;
    }

    pub fn plus_failure(&mut self) {
        self.failure += 1;
    }
}

