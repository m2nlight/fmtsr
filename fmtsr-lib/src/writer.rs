use std::io::Write;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

pub struct Writer {
    stdout: StandardStream,
    stderr: StandardStream,
    color_spec: ColorSpec,
}

enum StdType {
    StdOut,
    StdErr,
}

impl Writer {
    pub fn new() -> Writer {
        Writer {
            stdout: StandardStream::stdout(ColorChoice::Always),
            stderr: StandardStream::stderr(ColorChoice::Always),
            color_spec: ColorSpec::new(),
        }
    }

    pub fn info(&mut self, value: &str, print: bool) {
        self.write(value, None, StdType::StdOut, print);
    }

    pub fn warn(&mut self, value: &str, print: bool) {
        self.write(value, Some(Color::Yellow), StdType::StdOut, print);
    }

    pub fn success(&mut self, value: &str, print: bool) {
        self.write(value, Some(Color::Green), StdType::StdOut, print);
    }

    pub fn error(&mut self, value: &str, print: bool) {
        self.write(value, Some(Color::Red), StdType::StdErr, print);
    }

    fn write(&mut self, value: &str, color: Option<Color>, std_type: StdType, print: bool) {
        if print {
            self.color_spec.set_fg(color);
            match std_type {
                StdType::StdOut => {
                    let _ = self.stdout.set_color(&self.color_spec);
                    let _ = writeln!(&mut self.stdout, "{}", value);
                }
                StdType::StdErr => {
                    let _ = self.stderr.set_color(&self.color_spec);
                    let _ = writeln!(&mut self.stderr, "{}", value);
                }
            };
        }
    }

    pub fn reset(&mut self) {
        let _ = self.stdout.reset();
        let _ = self.stderr.reset();
    }
}
