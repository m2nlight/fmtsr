use super::result_count::ResultCount;
use super::writer::Writer;

pub struct Context {
    pub writer: Writer,
    pub result_count: ResultCount,
}

impl Context {
    pub fn new() -> Context {
        Context {
            writer: Writer::new(),
            result_count: ResultCount::new()
        }
    }
}