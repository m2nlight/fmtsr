use std::env;
use fmtsr_lib::context::Context;

fn main() {
    let mut ctx = Context::new();
    parse_args(&mut ctx);
    format_files(&mut ctx);
    print_count(&mut ctx);
    ctx.writer.reset();
}

fn parse_args(ctx: &mut Context) {
    let args: Vec<String> = env::args().collect();
    let args = format!("{:?}", args);
    ctx.writer.error(&args, true);
}

fn format_files(ctx: &mut Context) {
    ctx.result_count.plus_success();
    ctx.result_count.plus_failure();
}

fn print_count(ctx: &mut Context) {
    let result = format!(
        "\nSUCCESS: {}   FAIL: {}    COST: {}",
        ctx.result_count.success, ctx.result_count.failure, ctx.result_count.cost
    );
    ctx.writer.info(&result, true);
}
